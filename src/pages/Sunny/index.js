import { Text, View, ScrollView } from 'react-native'
import React, { Component } from 'react'

export class Sunny extends Component {
  render() {
    return (
      <View>
        <ScrollView>
        <Text style={{fontSize:20}}>Kisah Sukses Sunny Kamengmau, Pendiri Tas Robita</Text>
        <Text>Seorang tamatan SMP asal Nusa Tenggara Timur bernama Sunny Kamengmau berhasil menjadi seorang pengusaha sukses dengan mendirikan usaha Tas Robita bersama dengan partnernya bernama Nobuyuki Kakizaki, seorang pemilik perusahaan Real Pint Inc dari Jepang.</Text>
        <Text style={{fontSize:20}}>Mengenal Sunny Kamengmau</Text>
        <Text>Secara umum, bangku sekolah memang mengantarkan seorang individu untuk dapat meraih kesuksesan di kemudian hari yang tentu saja diimbangi dengan niat yang positif untuk mau berkembang dan maju. Namun, hal ini tidak berlaku bagi seorang yang berasal dari Nusa Tenggara Timur, Sunny Kamengmau.
            Pada usia 18 tahun, ia tidak menampatkan bangku sekolah SMA-nya. Sunny Kamengmau jenuh dengan pendidikan di sekolah pada saat itu. Dengan berbekal ijazah terakhir SMP-nya, ia merantau ke Pulau Bali.
            Di Bali, Sunny Kamengmau bekerja serabutan. Ia mau melakukan berbagai hal, diantaranya adalah buruh cuci mobil dan juga buruh renovasi rumah. Sampai pada akhirnya, ia bekerja menetap di sebuah Hotel bernama Uns Hotel yang terletak di Jalan Benesari, Legian, Kuta. Di tempat itu, ia bekerja sebagai tukang kebun selama satu tahun sebelum dipindahkan dan naik pangkat menjadi petugas keamanan atau security pada tahun 2000.</Text>
        <Text style={{fontSize:20}}>Melihat Peluang dari Kacamata Seorang Sunny Kamengmau</Text>
        <Text>Sunny Kamengmau senang bergaul dengan para tamu hotel di Uns Hotel dan hal ini membuatnya tertarik untuk mempelajari bahasa asing, terutama bahasa Inggris dan bahasa Jepang. Bahkan, dengan gajinya yang hanya Rp50 ribu, ia membelikannya sebuah kamus Bahasa Inggris. Ia melihat peluang tersendiri dari pekerjaannya sebagai security di hotel tersebut.
Kemauannya yang tinggi untuk belajar bahasa asing sangat menolongnya dalam memiliki relasi dengan para tamu hotel hingga pada suatu kesempatan, ia mendapatkan peluang bisnisnya melalui pertemuannya dengan Nobuyuki Kakizaki, seorang pengusaha dan pemilik dari Real Point Inc asal Jepang.
</Text>
        <Text style={{fontSize:20}}>Memulai Bisnis Tas Robita</Text>
        <Text>Berkat kefasihannya dalam berbahasa Jepang, Sunny Kamengmau bersahabat dengan Nobuyuki Kakizaki. Persahabatan mereka selama 5 tahun membuat mereka untuk bekerja sama sebagai partner usaha pembuatan tas.
        Mereka bekerja sama untuk membuat tas handmade di Indonesia yang akan di ekspor ke negeri asal Nobuyuki Kakizaki di Jepang. Mereka memilih produk handmade dikarenakan peminatan yang besar dari para konsumen di Jepang yang lebih menyukai produk handmade daripada tas produksi pabrik.
        entu saja ini menambah nilai harga dari tas tersebut oleh karena jumlah permintaan dan peminatan yang relatif tinggi untuk produk tas handmade.

Pada awal bisnis yang mereka jalani, Nobuyuki Kakizaki sering mengajaknya untuk membeli berbagai barang kerajinan tangan dan aksesoris di toko dan dijual kembali di Jepang. Sunny Kamengmau belajar banyak hal dari persahabatannya dengan Nobuyuki Kakizaki. Ia belajar untuk memilih berbagai barang  yang berkualitas dan cara mengirim barang ke Jepang. Saat itu, pekerjaan sebagai security di malam hari tidak ditinggalkan oleh Sunny Kamengmau.
Sunny Kamengmau dan Nobuyuki Kakizaki menamai usaha tas kulit mereka tersebut dengan nama Tas Robita dikarenakan kesukaan Nobuyuki Kakizaki terhadap karakter Nobita di serial kartun Doraemon.
        </Text>
        <Text style={{fontSize:20}}>Perkembangan Usaha Tas Robita</Text>
        <Text>Pada awal produksi Tas Robita, tidak banyak calon konsumen yang ingin memesan produk tersebut. Pemesanan pertama kali berasal dari Jepang dan hanya belasan yang dijual dengan omzet sebulan yang tidak menentu
        Namun, semangat mereka tidak pernah padam hingga bisnis mereka berkembang dan pada tahun 2007, produksi Tas Robita mencapai 5.000 tas setiap bulannya dan pada tahun 2009, Sunny Kamengmau bersama rekannya Nobuyuki Kakizaki merekrut setidaknya 300 orang karyawan. Suatu peningkatan usaha yang cukup signifikan karena pada saat awal mereka mendirikan bisnis Tas Robita pada tahun 2003, mereka hanya memiliki 20 karyawan saja.
        </Text>
        <Text style={{fontSize:20}}>Pengalaman Tak Terbayangkan dari Sunny Kamengmau</Text>
        <Text>Sunny Kamengmau sempat mengungkapkan bahwa pengalamannya bersama Nobuyuki Kakizaki dalam merintis usaha tersebut tidaklah terbayangkan sebelumnya. Sunny Kamengmau, seorang security memulai usaha tersebut tanpa modal, bahkan ia sendiri saat itu masih menyewa motor dan juga memiliki utang yang menumpuk. Namun jerih lelah mereka terbayarkan melalui niat usaha yang positif dan semangat yang tidak pernah padam sehingga ia mampu bangkit untuk mencapai kesuksesan.
        </Text>
        <Text style={{fontSize:20}}>Masa-masa Sulit Produksi Tas Robita</Text>
        <Text>Tidak ada sesuatu proses yang mulus dalam sebuah kehidupan dan juga dalam sebuah dunia entrepreneurship. Produksi Tas Robita mengalami penurunan hingga 3.500 tas setiap bulannya dikarenakan masalah sumber daya manusia. Selain itu, partner setia Sunny Kamengmau, yaitu Nobuyuki Kakizaki harus menutup usia karena penyakit kanker paru-paru yang ia derita.
        Namun, masa-masa sulit itu tetap ia hadapi dengan tegar dan tetap memandang masa depan yang lebih cerah. Usaha tersebut akhirnya tetap dilanjutkan oleh Sunny Kamengmau dan bekerja sama dengan istri dari Nobuyuki Kakizaki.

Sunny Kamengmau berhasil membuat berbagai inovasi dari produk Tas Robita dan melakukan berbagai kontrol produksi yang sangat ketat dari produk-produk yang dihasilkan. Berbekal pengalaman dan pembelajaran selama belasan tahun bersama almarhum partner setianya, membuat ia terus optimis akan kesuksesan dari bisnis usaha Tas Robita.
</Text>
        <Text style={{fontSize:20}}>Melirik Pasar Indonesia</Text>
        <Text>Keberhasilannya di negeri tetangga tak lantas membuatnya merasa puas. Sunny Kamengmau ingin merambah pasar di Tanah Air dengan mulai memperkenalkan produk Tas Robita melalui butik yang ia dirikan di Seminyak, Bali. Ia sangat optimis dengan produk Tas Robita hasil dari kerja samanya dengan almarhum Nobuyuki Kakizaki, sahabatnya.
        Dengan menguras setidaknya 100 juta rupiah, ia membangun interior seluas 30 meter persegi demi memperkenalkan butik Tas Robita di Seminyak, Bali. Rencananya, setelah keberhasilan yang akan ia capai di Bali, Sunny Kamengmau ingin merambah pasar di Ibu Kota. Ia sangat yakin bahwa Jakarta akan menjadi target pasarnya kemudian.
        </Text>
        </ScrollView>
      </View>
    )
  }
}

export default Sunny