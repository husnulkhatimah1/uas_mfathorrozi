import Splash from "./Splash";
import Home from "./Home";
import Nikola from "./Nikola";
import Bob from "./Bob";
import Susi from "./Susi";
import Reza from "./Reza";
import James from "./Home/James";
import Dea from "./Dea";
import Gibran from "./Gibran";
import Nicholas from "./Nicholas";
import Sukyanto from "./Sukyanto";
import Sunny from "./Sunny";

export {Splash, Home, Nikola, Bob, Susi, Reza, James, Dea, Gibran, Nicholas, Sukyanto, Sunny };