import { Text, View, ScrollView  } from 'react-native'
import React, { Component } from 'react'


export class Susi extends Component {
  render() {
    return (
      <View>
        <ScrollView>
        <Text style={{fontSize:20}}>Biografi Susi Pudjiastuti</Text>
        <Text>Susi Pudjiastuti lahir tanggal 15 Januari 1965 di Pangandaran, Jawa Barat. Ia merupakan putri dari Hajjah Suwuh Lasminah dan Ahmad Karlan. Sebenarnya, kedua orang tua Susi Pudjiastuti adalah orang Jawa Tengah.Akan tetapi, diketahui bahwa mereka menetap di Pangandaran, Jawa Tengah. Bahkan sudah lima generasi. Keluarga Susi memiliki usaha di bidang peternakan.
        Mereka melakukan transaksi jual beli ratusan hewan ternak. Hewan tersebut datang dari Jawa Tengah dan diperdagangkan di Jawa Barat. Rupanya, keluarga Susi bukan dari kalangan biasa.
Sang kakek, yaitu Haji Ireng dikenal sebagai tuan tanah di daerahnya.
Susi memiliki tiga orang anak. Pertama, Yoyok Yudi Suharyo. Kedua, Nadine Kaiser. Ketiga, Alvy Xavier.
        </Text>
        <Text style={{fontSize:20}}>Masa Sekolah Susi Pudjiastuti
</Text>
        <Text>Susi bersekolah seperti pada umumnya. Setelah tamat Sekolah Dasar dan Sekolah Menengah Pertama, ia melanjutkan pendidikannya. Kemudian ia melanjutkan pendidikan ke SMA Negeri 1 Yogyakarta.
        Pada masa bersekolah SMA, Susi sangat aktif dalam berkegiatan. Salah satunya adalah aktivitasnya dalam Gerakan Golput. Ternyata, keaktifannya tersebut justru menyita waktu sekolahnya.
Sampai akhirnya ia keluar dari SMA pada kelas 2. Putus sekolah tidak membuat Susi menjadi malas dan menyerah. Ia memilih untuk menjual perhiasan miliknya.
Dari penjualan tersebut, ia berhasil mengumpulkan uang sebesar Rp. 750.000. Uang tersebut ternyata dijadikan modal oleh Susi. Rupanya ia memilih untuk terjun ke dunia bisnis setelah keluar dari sekolah.
        </Text>
        <Text style={{fontSize:20}}>Susi Pudjiastuti Mulai Berbisnis</Text>
        <Text>Modal yang sudah ia kumpulkan tersebut dipakai untuk berbisnis ikan. Ia menjadi seorang pengepul atau tengkulak ikan. Susi memulai bisnisnya itu pada tahun 1983 di Pangandaran.
        Mulanya, ia hanya berjualan ikan dengan menggunakan sepeda saja. Susi berkeliling pantai untuk menjual ikan. Semua hasil dan keuntungannya ia kembangkan lagi.
Berkat kegigihan dan kepintarannya dalam berbisnis, pada tahun 1996 Susi berhasil mendirikan sebuah pabrik. Ia mendirikan pabrik pengolahan ikan. Pabrik tersebut ia beli nama PT. ASI Pudjiastuti Marine Product.
Produk unggulan dari pabrik tersebut adalah lobster dengan merek “Susi Brand”. Semakin lama, bisnis pengolahan ikan milik Susi tersebut semakin berkembang.
Pada mulanya, ia berbisnis di satu wilayah saja. Setelah itu ia mulai mengembangkan ke wilayah lain. Tidak hanya sampai situ, bahkan dari satu daerah ke daerah lain. Sampai pada akhirnya ia berhasil menembus pasar Asia dan Amerika.
Perkembangan bisnisnya tersebut membuatnya membutuhkan transportasi yang lebih lagi. Hal itu untuk kepentingan produk hasil lautnya. Supaya tetap segar dan cepat sampai.</Text>
        <Text style={{fontSize:20}}>Susi Pudjiastuti dan Bank Mandiri
</Text>
        <Text>Pada tahun 2004, Bank Mandiri mempercayainya dan memberikan pinjaman dana kepadanya. Pada saat itu, Bank Mandiri meminjamkan dana sebesar USD 4,7 juta. Angka segitu sama seperti sekitar 47 miliar.
        Kemudian uang tersebut digunakan untuk membangun sebuah landasan. Selain itu, ia juga membeli pesawat Cessna Grand Caravan.
Susi memutuskan untuk membeli Cessna Grand Caravan seharga 20 miliar. Uang yang digunakan tersebut adalah uang pinjaman dari bank.
Kemudian Susi mendirikan PT. ASI Pudjiastuti Aviation.Melalui PT. ASI Pudjiastuti Aviation yang didirikannya tersebut, satu-satunya pesawat yang ia miliki tersebut digunakan untuk mengangkut hasil laut. Ia mengirimkan lobster dan berbagai ikan segar, hasil tangkapan dari nelayan di berbagai pantai Indonesia.
Ia mengirimkan hasil laut tersebut ke pasar Jakarta dan Jepang. Call Sign yang digunakan pesawat Cessna tersebut adalah Susi Air.
        </Text>
        <Text style={{fontSize:20}}>Biografi Susi Pudjiastuti</Text>
        <Text>Awalnya Hanya Berniat Membantu</Text>
        <Text>Pada saat itu, terjadi bencana tsunami di Aceh. Kemudian Susi memutuskan untuk mengirim pesawatnya kesana. Ia berniat untuk membantu para korban tsunami tersebut.
Pada tanggal 27 Desember, pesawat milik Susi mendarat pertama kali di Meulaboh. Kemudian keesokan harinya mendarat lagi dengan membawa bahan-bahan makanan dan berbagai kebutuhan. Seperti beras, mie instan, air, tenda dan lain lain.
Pada saat itu, ia hanya berniat untuk membantu para korban. Pesawatnya itu digunakan untuk mengangkut sandang dan pangan untuk para korban. Akan tetapi, dibalik niat mulianya tersebut, Susi mendapatkan balasannya.
Ketika hendak kembali, ternyata banyak lembaga non-pemerintah yang memintanya tetap berpartisipasi dalam recovery di Aceh. Mereka ingin membayar sewa dari pesawat tersebut. Setelah itu, sekitar sekitar 1,5 tahun pesawat tersebut bekerja di Aceh. Rupanya, hasil dari 1,5 tahun tersebut bisa membuat Susi membeli pesawat lagi.
Tidak hanya sampai disitu. Melalui hal itu, nama bisnis Susi semakin berkembang. Peristiwa tersebut membuat Susi mengubah arah bisnisnya.
Ketika bisnis ikan sedang mengalami penurunan, Susi memutuskan untuk menyewakan pesawatnya. Pesawat yang semula digunakan untuk mengangkut ikan, ia sewakan untuk misi kemanusiaan.
Selama tiga tahun berjalan, perusahaan penerbangan tersebut semakin berkembang. Bahkan telah memiliki total 14 pesawat. 4 pesawat ada di Papua, 4 pesawat ada di Balikpapan, Sumatera dan Jawa.
Bisnisnya tersebut terus berkembang. Sampai perusahaannya memiliki total 32 pesawat Cessna Grand Carravan, 1 pesawat Diamond star, 9 pesawat Pilatus Porter, dan 1 pesawat Diamond Twin Star. Saat ini, Susi Air sudah mengoperasikan 50 pesawat terbang dengan berbagai jenis.
Kemudian pada tahun 2008, ia mulai menhembangkan bisnis aviasinya. Susi membuka sekolah pilot yang bernama Susi Flyung School. Melalui PT. ASI Pudjiastuti Flying School.
Menteri Kelautan dan Perikanan (Kabinet Kerja 2014 – 2019)
Menteri Kelautan dan Perikanan (Kabinenet Kerja 2014 - 2019)Semua kesuksesan yang diraihnya membawanya sebagai Menteri Kelautan dan Perikanan. Pada tanggal 26 Oktober 2014, ia ditetapkan sebagai Menteri Kelautan dan Perikanan dalam Kabinet Kerja Joko Widodo dan Jusuf Kalla.
Akan tetapi, sebelum dilantik Susi melepaskan semua posisinya. Posisi di perusahaan penerbangan Susi Air dan beberapa posisi lain yang ia jalankan. Termasuk sebagai Presiden Direktur di PT. ASI Pudjiastuti, yang bergerak di bidang perikanan.
Ia juga meninggalkan posisinya di PT. ASU Pudjiastuti Aviation yang bergerak pada bidang penerbangan. Hal itu dilakukannya supaya menghindari konflik kepentingan di antara dirinya sebagai pemimpin bisnis dan sebagai menteri.
Selain itu, alasan lain ia melepas demua jabatannya adalah supaya ia dapat bekerja secara maksimal di pemerintahan. Khususnya pada bidang perikanan dan keluatan.

</Text>
        
        </ScrollView>
      </View>
    )
  }
}

export default Susi