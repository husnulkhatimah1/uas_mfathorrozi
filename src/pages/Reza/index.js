import { Text, View, ScrollView } from 'react-native'
import React, { Component } from 'react'


export class Reza extends Component {
  render() {
    return (
      <View>
        <ScrollView>
        <Text>Reza Nurhilman, pengusaha sukses asal Bandung, ini terkenal dengan bisnis keripiknya yang melegenda. Sosok Reza dikenal sebagai salah satu profil orang sukses di Indonesia yang berhasil menghasilkan omzet hingga Rp 7 miliar per bulan.
        Keripik Maicih pertama kali muncul di 2010 dengan promosi memanfaatkan media sosial Twitter. Mereka menggunakan strategi jualan ‘mendadak’ di beberapa wilayah khususnya Bandung dengan mobil si penjual.
        Sampai akhirnya bisa menjadi salah satu brand keripik ternama di masyarakat yang diingat sampai skarang dengan level kepedasan 1-10. 
        </Text>
        <Text style={{fontSize:20}}>Awal mula bisnis keripik Maicih </Text>
        <Text>Reza Nurhilman atau biasa dipanggil AXL sudah mulai usaha sejak di bangku sekolah. Pada mulanya usaha Maicih terinspirasi dari jajanan keripik pedas yang dijual seorang nenek yang tinggal di sekitar rumahnya. Saat itu ia hanya langganan untuk membeli keripik tersebut sebelum akhirnya berani menjual ke teman-teman di sekolah. Responsnya ternyata baik hingga Reza dapat memberi label merk ke bisnis keripiknya dengan sebutan “Maicih”
Menurutnya kata Maiicih terinspirasi dari orang yang membuat keripiknya karena berpenampilan seperti emak-emak. Selain itu, merk tersebut juga datang dari sang Ibu yang yang sering menyebut dompet kecil untuk menyimpan emas dengan panggilan Maicih.
Di tahun 2010 ia mulai mencoba berkreasi dan membuat resep cemilan keripik singkong dengan menu utama yang menonjolkan rasa pedas. Ide itu pun berlanjut ketika dirinya menjalin kerja sama dengan sebuah perusahaan cemilan yang ada di Kota Cimahi.
Dengan modal awal Rp 15 juta, akhirnya keripik Maicih generasi pertama diproduksi sebanyak 50 bungkus per harinya. Tidak butuh waktu lama, hanya berselang beberapa bulan saja produk Maicih mulai populer di Bandung dan akhirnya ke seluruh wilayah Indonesia.
</Text>
        <Text style={{fontSize:20}}>Strategi pemasaran yang tepat</Text>
        <Text>Promosi yang dilakukan Reza Nurhilman pada produknya dinilai sangat tepat, karena ia memanfaatkan media sosial yang saat itu sedang booming di Indonesia, yaitu Twitter dan Facebook.
Caranya yang lain, konsumen Maiicih dapat melihat lokasi para agen (reseller) secara langsung yang tersebar di tempat-tempat keramaian seperti kampus, kantor dan sekolah.
</Text>
        <Text style={{fontSize:20}}>Prinsip yang diterapkan Reza dalam berbisnis</Text>
        <Text>Beberapa prinsip Reza dalam berbisnis bisa kamu terapkan untuk memulai usaha, seperti jangan pikir untung rugi terlebih dahulu, lakukan saja dahulu dengan totalitas. Menurut Reza memulai berbisnis artinya berani mengambil risiko.
Selain itu,pembuat usaha juga harus kreatif agar menciptakan personal branding yang mudah diingat konsumen, apalagi melihat persaingan yang sangat kompetitif di industri kuliner Tanah Air.</Text>
        
        </ScrollView>
      </View>
    )
  }
}

export default Reza