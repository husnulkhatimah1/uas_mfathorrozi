import { Text, View, ScrollView } from 'react-native'
import React, { Component } from 'react'


export class Bob extends Component {
  render() {
    return (
      <View>
        <ScrollView>
        <Text style={{fontSize:17}}>Mengenal Lebih Dekat Pengusaha Sukses Bob Sadino</Text>
        <Text>Bambang Mustari yang kemudian akrab dikenal Bob Sadino adalah sosok inspiratif kelahiran Tanjungkarang, Lampung Tanggal 9 Maret 1933. Bob Sadino merupakan anak bungsu dari lima bersaudara dari pasangan Bapak Sadino dan Ibu Itinah Soeraputra.
        Ia dibesarkan oleh keluarga yang berkecukupan dengan Sang Ayah yang berprofesi guru dan kepala sekolah saat zaman Belanda dahulu. Kondisi keluarganya yang dibilang mapan pada saat itu tak membuat Bob Sadino menjadi manja dan menggantungkan hidupnya pada keluarga melainkan ia tumbuh mandiri dan kompeten.
        Saat usianya masih 19 tahun, Bob Sadino telah kehilangan Sang Ayah meninggal dunia yang juga meninggalkan harta warisan untuk dirinya.  Privilege yang sudah dimiliki Bob Sadino tidak membuatnya mudah puas dan mudah menyerah.
        Riwayat pendidikan Bob Sadino tercatat ia pernah mengenyam pendidikan di Sekolah Dasar Yogyakarta tahun 1947, SMP di Jakarta tahun 1950, dan SMA di Jakarta tahun 1953. Setelah lulus dari bangku SMA perjalanan karir Bob Sadino pun dimulai.
        Bob Sadino memilih untuk tidak melanjutkan pendidikannya ke perguruan tinggi karena menurutnya seorang mahasiswa ibarat seorang pemulung yang memunguti barang-barang yang hanya akan memenuhi otaknya.
        Jika kebanyakan orang mencari ilmu pengetahuan melalui lembaga pendidikan secara bertahap hingga menjadi seorang ahli, maka berbeda dengan Bob Sadino yang memilih belajar dari pengalaman yang ia lakukan secara mandiri.
        Sejak kepergian Sang Ayahlah Bob Sadino mulai terjun sebagai pribadi yang mandiri untuk bertahan hidup. Perjalanan karirnya ia mulai dari nol dengan mengambil setiap kesempatan yang mendatanginya.
        </Text>
        <Text style={{fontSize:20}}>Kisah Sukses Bob Sadino</Text>
        <Text>Kisah Sukses Bob Sadino</Text>
        <Text>Setelah lulus SMA dan enggan melanjutkan kuliah, Bob Sadino akhirnya memutuskan untuk bekerja di Unilever beberapa tahun sebelum ia singgah dan bekerja di luar negeri. Pada tahun 1955 Bob Sadino mengundurkan diri sebagai karyawan Unilever dan pergi keluar negeri.</Text>
        <Text>2. Kerja Di Jakarta Lylod  di Belanda dan Jerman</Text>
        <Text>Setelah memutuskan keluar dari Unilever, Bob Sadino pergi keliling dunia dengan uang warisan sampai akhirnya ia memutuskan untuk menetap di Belanda. Selama 9 tahun di Belanda ia bekerja di perusahaan pelayaran Djakarta Lylod di Kota Amsterdam dan Jerman.
Di Belanda pulalah Bob Sadino bertemu dengan tambatan hatinya bernama Soelami Soejoed yang akhirnya menjadi istrinya yang merupakan seorang karyawati Bank Indonesia Amerika Serikat.
</Text>
        <Text>3. Membuka Sewa Mobil dan Menjadi Supir</Text>
        <Text>Usaha jatuh bangunnya seorang Bob Sadino adalah ketika ia menjadi sopir setelah kepulangannya dari Belanda sebagai perantau.
Berbekal gaji hasil kerja di Eropa dan sisa warisan keluarganya, Bob Sadino membuka usaha mobil dan ia sendiri yang menjadi supir. Usahanya tersebut tidak berjalan lancar setelah musibah kecelakan menimpa Bob Sadino yang mengakibatkan mobilnya rusak parah.
</Text>
        <Text>4. Menjadi Kuli Bangunan</Text>
        <Text>Akibat musibah yang menimpanya, Bob Sadino mengalami kerugian besar dan tidak bisa melanjutkan usaha sewa dan memperbaiki mobilnya. Desakan kesulitan itu akhirnya membuat Bob Sadino menjadi kuli bangunan untuk menyambung hidup dirinya dan keluarganya.
Dengan banyaknya musibah yang menimpa diri Bob Sadino, beliau tetap bangkit yang membuatnya sukses hingga saat ini. Motivasi tersebut yang harus kita semua miliki khususnya bagi kalian yang ingin memulai bisnis seperti halnya pada buku 33 Cara Kaya Ala Bob Sadino, Motivasi Bisnis Anti-Gagal.
Kesulitan ini sempat membuat Bob Sadino mengalami tekanan dan depresi karena gaji yang ia terima sangat pas-pasan untuk memenuhi kebutuhan keluarganya.
</Text>
        <Text>5. Bisnis Telur Ayam Negeri</Text>
        <Text>Bob Sadino memiliki rekan pensiunan Jenderal Angkatan Udara bernama Sri Mulyono Herlambang yang juga merupakan perintis usaha ayam ras. Ia adalah pendiri Perhimpunan Peternak Unggas Indonesia (PPUI) yang kemudian menyarankan Bob Sadino untuk memelihara ayam.
Saran rekannya itu akhirnya memunculkan ide Bob Sadino untuk memiliki usaha ternak ayam dengan hasil temuan baru. Jika ukuran telur ayam lokal yang sebelumnya berukuran lebih kecil dibandingkan ukuran telur ayam di luar negeri.
Bob Sadino kemudian mengembangbiakan ayam broiler yang ia pelajari dari majalah peternakan yang berbahasa Belanda. Mengingat ia bukalah lulusan ilmu peternakan membuat Bob Sadino mencari sumber belajar.
Pertama Bob Sadino mencoba menawarkan telur ayam negeri ke tetangga-tetangganya di daerah  Kemang, Jakarta. Tetangganya di daerah tersebut kebanyakan adalah ekspatriat yang kemudian ia tawari dari pintu ke pintu. Berkat ketekunan dan keuletannya dalam belajar dan melihat peluang membuat penjualan telur ayam negerinya meningkat menjadi puluhan kilo.
Untuk merintis usaha telur ayam negeri ini, Bob Sadino didampingi oleh sang istri yang menjalani tantangan usahanya bersama-sama. Bob Sadino tak jarang memperoleh makian, teguran, bahkan pantauan dari orang asing yang ingin bersaing usaha dengannya. Dari tantangan itulah Bob Sadino dan Sang Istri terus memperbaiki kualitas untuk produk dan pelayanan mereka.
</Text>
        <Text>6. Bob Sadino Bisnis Sayuran</Text>
        <Text>Usaha telur ayam negerinya yang terus meningkat membuat Bob Sadino tidak membuatnya mudah puas. Ia pun akhirnya mengembangkan bisnisnya ke bisnis sayuran. Ia berhasil melihat peluang dengan mengembangkan sayur mayur dan buah-buahan luar negeri yang belum ada di Indonesia.
Sayur yang dikenalkan Bob Sadino adalah jagung manis, melon, dan brokoli. Kesempatan peluang yang besar membuat penjualannya meningkat drastis.
Keberhasilannya ini justru merubah kepribadian Bob Sadino yang sebelumnya menjalankan bisnis dengan cara feodal menjadi bisnis pelayan. Bob Sadino menganggap bahwa untuk menuju sukses sebuah usaha pasti selalu diawali dengan kegagalan demi kegagalan.
Jalan terjal wirausaha ini lah yang Bob Sadino lalui hingga ia menganggap bahwa kegagalan itu adalah hal yang biasa dan uang bukanlah hal nomor satu baginya.
Hal utama menurut Bob Sadino adalah kemauan, komitmen, dan keberanian untuk mengambil peluang.  Perubahan cara bisanis yang ia lakukan inilah yang justru membuat bisnisnya semakin meningkat dan berkembang.
Bob Sadino bahkan tidak hanya menjual sayuran saja, namun ia juga mengenalkan cara berkebun hidroponik di Indonesia untuk menghasilkan sayuran segar dengan cara yang mudah.
Kesuksesan bisnisnya di bidang peternakan tak kunjung juga membuatnya puas dan akhirnya Bob Sadino mencoba merambah agribisnis hortikultura yang mengelola kebun-kebun sayur mayur untuk konsumsi orang asing di Indonesia.
Pada saat itu cara berkebun yang dilakukan Bob Sadino belum ada yang menerapkannya sama sekali. Ia pun akhirnya mengajak petani-petani lokal untuk bekerja sama mengembangkan cara berkebun dengan konsep yang dinamakan Kem Fam.
Bisnis sayurannya yang sukses rupanya tidak membuat Bob Sadino untuk berhenti mengembangkan usaha. Ia pun membuat supermarket bernama Kem Chicks yang kembangkan dari konsep bisnis Kem Fam-nya.
</Text>
        <Text>7. Memiliki Supermarket Kem Chicks</Text>
        <Text>Kem Chicks adalah supermarket bentukan Bob Sadino pada tahun 1970 yang menyediakan beragam produk pangan impor yang disediakan untuk masyarakat Jakarta. Lokasi supermarket ini berada di Jl Kemang Raya, Nomor 3-5, Jakarta Selatan.
Berdirinya supermarket Kem Chicks semakin membuat bisnis Bob Sadino bersinar, hingga akhirnya ia kembali memanfaatkan peluang untuk membuat produk sosis karena permintaan yang meningkat.
Tahun 1975 Bob Sadino membuat perusahaan KemFood yang menjadi pelopor industri daging olahan di Indonesia pertama kali. Produk Kemfood antara lain burger, bakso, nugget, dan berbagai jenis olahan daging lainnya.
Tercatat pada tahun 1985, Perusahaan Bob Sadino rata-rata penjualannya konsisten berkisar 40 sampai 50 ton daging segar, 60 sampai 70 ton daging olahan, dan 100 ton sayur per tahun.
Berkat ide-ide kreatifnya, Bob Sadino menjadi sosok yang sangat dikagumi oleh masyarakat karena sangat menginspirasi orang banyak. Bob Sadino memang sosok pionir bangsa yang mengenalkan ayam negeri dan menjadi orang pertama yang menggunakan perkebunan sayur hidroponik dan mengenalkan ke masyarakat luas.
</Text>
        <Text style={{fontSize:20}}>Daftar Bisnis Milik Bob Sadino</Text>
        <Text>1. Supermarket Kem Chicks</Text>
        <Text>Didirikan pada tahun 1970  oleh Bob Sadino yang menyediakan berbagai produk impor untuk orang Jakarta. Target pasar supermarket ini adalah para eksplisit atau orang asing dan masyarakat kelas menengah ke atas di Jakarta.</Text>
        <Text>2. Perusahaan Kemfood</Text>
        <Text>Perusahaan Kemfood atau Kemang Food Industries adalah perusahaan pengolahan daging milik Bob Sadino yang bermodal dari Penanaman Modal Dalam negeri (PMDN). PT Boga Catur Rata adalah pemegang saham tunggal perusahaan ini.
Bob Sadino mendirikan perusahaan ini karena ia melihat peluang dengan banyaknya permintaan daging dan sosis pada tahun 1975. Perusahaan ini berlokasi di Jl. Pulo Kambing Nomor 11 Jakarta, Industrial Estate Pulogadung, Jakarta Timur.
</Text>
        <Text>3. Kem Farm</Text>
        <Text>Kem Farm adalah perkebunan saur milik Bob Sadino yang berfokus pada agribisnis sayuran dan buah-buahan. Produk dari perkebunan ini bahkan dijual untuk ekspor, salah satunya ke Jepang hingga mencapai 10.000 ton buah dan sayuran per bulan. Terletak di Jl Jendral Gatot Subroto Kawasan Industri Candi BI VIII/16A, Semarang, perkebunan Kemfarm ini sukses dengan konsep hidroponiknya.</Text>
        <Text>4. The Mansion at Kemang</Text>
        <Text>Selain di bidang peternakan dan perkebunan, Bob Sadino juga memiliki bisnis di bidang properti. Perusahaan tersebut bernama The Mansion at Kemang yang berkonsep apartemen, pusat perbelanjaan, dan perkantoran yang berada di satu lokasi.
Perusahaan ini memiliki 32 lantai dengan 180 unit apartemen, 10 unit perkantoran yang letaknya dekat dengan perusahaan Kem Chicks miliknya.
</Text>
        </ScrollView>
      </View>
    )
  }
}

export default Bob