import { Text, View, ScrollView } from 'react-native'
import React, { Component } from 'react'

export class Sukyanto extends Component {
  render() {
    return (
      <View>
        <ScrollView>
        <Text style={{fontSize:20}}>Sukyatno, Pendiri Es Teler 77 yang Pernah Jadi Calo SIM & Tak Lulus SMP</Text>
        <Text>Sebagian milenial yang biasa berjalan-jalan ke mall, tentu tidak asing dengan restoran Es Teler 77. Restoran yang terkenal dengan Es Telernya tersebut memang cukup melegenda. Namun di balik nama besar Es Teler 77, terdapat sosok yang berjasa dalam perjalanannya hingga sukses seperti saat ini.
                Dialah mendiang Sukyatno Nugroho, pendiri sekaligus Eks Presiden Komisaris Es Teler 77. Lahir pada 3 Agustus 1948, pria asal Pekalongan tersebut nyatanya punya sejumlah pengalaman pahit sebelum berhasil bersama Es Teler 77.
                Meski berasal dari kalangan menengah, Sukyatno hanya bersekolah hingga jenjang SMP akibat kurang cakap secara akademik. Ia bahkan pernah tidak naik kelas hingga dua kali semasa bersekolah. Sadar tidak suka sekolah, akhirnya ia mengurungkan rencana untuk meneruskan ke SMA dan lebih memilih untuk belajar berdagang.
                Selepas itu, ia dikirim ke rumah pamannya untuk dididik perihal berdagang. Setelah beberapa waktu bersama pamannya, ia langsung bekerja menjadi salesman alat elektronik. Di saat ini pula ia bertemu istrinya Yenny Widjaja yang merupakan sesama pedagang alat elektronik.
                Setelah pernikahannya pada 1971, berbagai pekerjaan ia geluti, dari calo SIM, hingga memborong pembangunan perumahan. Sialnya, ia mengalami kerugian besar akibat sejumlah kesalahan teknis. Ia bangkrut karena hutang-hutangnya. Kemiskinan bahkan membuatnya tak bisa menyekolahlan anaknya.
                Saat inilah semuanya bermula, saat dimana ia memutuskan untuk berjualan es teler. Bisnisnya ini dibekali modal Rp 1 juta beserta rese es teler andalan mertuanya yang pernah memenangi lomba majalah Gadis. Bisnisnya ia namakan dengan "Es Teler 77," angka keberuntungan kata Sukyatno.
                Berawal dari sebuah kedai kecil di emperan pusat perbelanjaan Duta Merlin, Jakarta Pusat. Kedai kecilnya ini  tidak bertahan lama lantaran perlakuan semena-mena dari pihak manajemen gedung. Sukyatno dan Istrinya yang tak mampu membayar sewa akhirnywaraa terpaksa tutup.
                Tak menyerah, mereka membuka Es Teler 77 di tempat lain yang lebih bersahabat, yakni di sebelah gedung pertokoan Gajah Mada Plaza. Sejak itulah Es Teler 77 mulai berkembang pesat. Mertuanya kemudian mendirikan perusahaan CV Es Teler 77 guna membantu melebarkan sayap bisnis Sukyatno tersebut.
                Meski sudah cukup besar dan memiliki beberapa cabang di pusat perbelanjaan Ibu Kota, Es Teler 77 sebagai produk lokal kerap memperoleh ketidakadilan. Saat itu, kebanyakan pihak pemilik lapak lebih memprioritaskan restoran asing karena dianggap memiliki keuntungan yang lebih besar.
                Namun, hal tersebut tidak mematahkan semangat Sukyatno dan Keluarganya. Berbagai jalan terus dicoba, hingga akhirnya muncul ide penggunaaan sistem waralaba pada 1987. Saat itulah, cabang pertamanya di Solo berhasil dibuka sebagai cabang pertama di luar Jakarta.
                Sejak pembukaan pertama di luar domisili, nama Es Teler 77 semakin dikenal. Banyak pihak yang tertarik untuk bekerja sama pada sistem waralaba dari Sukyatno ini. Para peminat dari berbagai daerah tersebut akhirnya turut membuka cabang-cabang baru di berbagai kota.
                Cabang tersebut bahkan hampir mencapai ratusan di seluruh Indonesia. Hal ini sampai membuat Sukyatno harus membuka dapur pusat di Jakarta Barat pada 1997 sebagai pusat distribusi kebutuhan banyak gerai tersebut. Dapur ini kemudian berpindah ke Tangerang.
                Hingga saat ini, Es Teler 77 begitu dikenal dan mudah ditemukan di banyak pusat perbelanjaan. Hebatnya, gerai Es Teler 77 acapkali ditemukan berdiri kokoh di antara gerai-gerai besar milik asing seperti KFC, McDonald's, dan lain-lain.
                Sang pendiri, Sukyatno Nugroho akhirnya menyerahkan bisnisnya ini kepada anak-anaknya, hingga akhirnya meninggal pada 11 Desember 2007. Ia meninggalkan sekitar 300 cabang Es Teler di seluruh Indonesia dan di luar negeri seperti Australia, Malaysia, dan Singapura yang secara keseluruhan beromzet sekitar Rp 180 miliar setiap bulannya</Text>
        </ScrollView>
      </View>
    )
  }
}

export default Sukyanto