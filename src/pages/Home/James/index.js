import { Text, View, ScrollView } from 'react-native'
import React, { Component } from 'react'


export class James extends Component {
  render() {
    return (
      <View>
        <ScrollView>
        <Text>James Prananto merupakan pendiri merek kopi terkenal yaitu Kopi Kenangan yang menjadi inspirasi sukses banyak orang. Kopi Kenangan sendiri merupakan perusahaan Food and Beverage yang menjadi unicorn pertama di bidang makanan dan minuman di Asia Tenggara.
            Saat ini Kopi Kenangan sendiri merupakan merek kopi yang disenangi semua kalangan di Indonesia. Gerai Kopi Kenangan di seluruh Indonesia kurang lebih ada 600 gerai dan tersebar di berbagai kota.</Text>
        <Text style={{fontSize:20}}>Kisah di Balik Berdirinya Kopi Kenangan</Text>
        <Text>Kopi Kenangan tidak hanya didirikan oleh James Prananto. Edward Tirtanata adalah teman sekaligus partner bisnis dari James Prananto yang dipertemukan saat menempuh perkuliahan di University of South California (USC), Amerika Serikat. Sebelum mendirikan Kopi Kenangan, mereka berdua sempat mendirikan sebuah kedai teh premium di Jakarta bernama Lewis & Carroll (L&C).
        Bisnis tersebut cukup sulit kala itu, karena harga teh per cangkirnya berada di harga Rp40-Rp60 ribu yang terlalu mahal bagi sebagian orang Indonesia dan tidak terlalu menghasilkan bagi mereka. Belajar dari hal tersebut, Edward berusaha untuk lebih mengamati tren saat ini. Ia lalu menyadari bahwa banyak orang Indonesia yang gemar meminum kopi.
        Dari situlah, ide untuk mendirikan Kopi Kenangan lahir. Edward pun mengajak James untuk membuat sebuah kedai kopi yang menjual kopi berkualitas dengan harga yang lebih terjangkau.</Text>
        <Text style={{fontSize:20}}>Kisah Sukses James Prananto</Text>
        <Text>Berangkat dari kegagalan, James Prananto bangkit dan mendirikan Kopi Kenangan bersama Edward Tirtanata. Bersama temannya, James membuka kedai Kopi Kenangan pertama di tahun 2017 dan mendapat sambutan yang cukup baik.Kedai tersebut awalnya hanya menjual sekitar 700 cangkir kopi saja. Seiring berjalannya waktu, bisnis tersebut berkembang hingga berhasil menjual hingga 3 juta cangkir kopi tiap bulannya. Mulai dari situ, bisnis kopi tersebut mendapatkan pendanaan awalnya sebesar USD8 juta atau sekitar Rp114,8 miliar dari Alpha JWC Venture.
        Setelah mendapatkan suntikan dana tersebut, Kopi Kenangan semakin berkembang dan memiliki lebih dari 60 gerai. Pada Desember 2021 lalu, Kopi Kenangan mendapatkan pendanaan sebesar USD96 juta atau setara dengan Rp1,37 triliun dan menjadikannya sebagai bisnis F&B pertama di Asia Tenggara yang menyandang gelar Unicorn.
        </Text>
        <Text></Text>
        <Text></Text>
        <Text></Text>
        </ScrollView>
      </View>
    )
  }
}

export default James