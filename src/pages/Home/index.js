import { Text, View, ScrollView, TouchableOpacity, Image } from 'react-native'
import React, { Component } from 'react'

export class Home extends Component {
  render() {
    return (
      <View style={{marginBottom:70}}>
               <View>
                  <Text style={{textAlign:'center', fontSize:20}}>KISAH INSPIRASI</Text>
               </View>
               <View>
                <ScrollView>
                  <View style={{flex:2,flexDirection:'row',borderColor:'black', justifyContent:'center'}}>
                    <View style={{height: 120, width: 355,backgroundColor: 'darkgrey',borderRadius: 10, borderColor:'black',borderEndWidth:8, marginTop:10}}>
                    <TouchableOpacity  onPress={() => this.props.navigation.navigate('Nikola')}>
                      <View style={{flexDirection:'row'}}> 
                        <Image source={require('../../image/tesla.png')} style={{height:100, width:100, marginTop:10, marginLeft:10, borderRadius:10}}/>
                        <View><Text style={{fontSize:20, marginTop:60, marginLeft:10, color:'black'}}>Nikola Tesla</Text></View>
                      </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View style={{flex:2,flexDirection:'row',borderColor:'black', justifyContent:'center'}}>
                    <View style={{height: 120, width: 355,backgroundColor: 'darkgrey',borderRadius: 10, borderColor:'black',borderEndWidth:8, marginTop:10}}>
                      <TouchableOpacity onPress={() => this.props.navigation.navigate('Bob')}>
                      <View style={{flexDirection:'row'}}> 
                        <Image source={require('../../image/bob.png')} style={{height:100, width:100, marginTop:10, marginLeft:10,borderRadius:10}}/>
                        <View><Text style={{fontSize:20, marginTop:60, marginLeft:10, color:'black'}}>Bob Sadino</Text></View>
                      </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View style={{flex:2,flexDirection:'row',borderColor:'black', justifyContent:'center'}}>
                    <View style={{height: 120, width: 355,backgroundColor: 'darkgrey',borderRadius: 10, borderColor:'black',borderEndWidth:8, marginTop:10}}>
                     <TouchableOpacity onPress={() => this.props.navigation.navigate('Susi')}>
                      <View style={{flexDirection:'row'}}> 
                        <Image source={require('../../image/susi.png')} style={{height:100, width:100, marginTop:10, marginLeft:10, borderRadius:10}}/>
                        <View><Text style={{fontSize:20, marginTop:60, marginLeft:10, color:'black'}}>Susi Pudjiastuti</Text></View>
                      </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View style={{flex:2,flexDirection:'row',borderColor:'black', justifyContent:'center'}}>
                    <View style={{height: 120, width: 355,backgroundColor: 'darkgrey',borderRadius: 10, borderColor:'black',borderEndWidth:8, marginTop:10}}>
                     <TouchableOpacity onPress={() => this.props.navigation.navigate('Reza')}>
                      <View style={{flexDirection:'row'}}> 
                        <Image source={require('../../image/reza.png')} style={{height:100, width:100, marginTop:10, marginLeft:10, borderRadius:10}}/>
                        <View><Text style={{fontSize:20, marginTop:60, marginLeft:10, color:'black'}}>Reza Nurhilman</Text></View>
                      </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View style={{flex:2,flexDirection:'row',borderColor:'black', justifyContent:'center'}}>
                    <View style={{height: 120, width: 355,backgroundColor: 'darkgrey',borderRadius: 10, borderColor:'black',borderEndWidth:8, marginTop:10}}>
                     <TouchableOpacity onPress={() => this.props.navigation.navigate('James')}> 
                      <View style={{flexDirection:'row'}}> 
                        <Image source={require('../../image/james.png')} style={{height:100, width:100, marginTop:10, marginLeft:10, borderRadius:10}}/>
                        <View><Text style={{fontSize:20, marginTop:60, marginLeft:10, color:'black'}}>James Prananto</Text></View>
                      </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View style={{flex:2,flexDirection:'row',borderColor:'black', justifyContent:'center'}}>
                    <View style={{height: 120, width: 355,backgroundColor: 'darkgrey',borderRadius: 10, borderColor:'black',borderEndWidth:8, marginTop:10}}>
                     <TouchableOpacity onPress={() => this.props.navigation.navigate('Dea')}>
                      <View style={{flexDirection:'row'}}> 
                        <Image source={require('../../image/dea.png')} style={{height:100, width:100, marginTop:10, marginLeft:10, borderRadius:10}}/>
                        <View><Text style={{fontSize:20, marginTop:60, marginLeft:10, color:'black'}}>Dea Valencia</Text></View>
                      </View>
                      </TouchableOpacity> 
                    </View>
                  </View>
                  <View style={{flex:2,flexDirection:'row',borderColor:'black', justifyContent:'center'}}>
                    <View style={{height: 120, width: 355,backgroundColor: 'darkgrey',borderRadius: 10, borderColor:'black',borderEndWidth:8, marginTop:10}}>
                      <TouchableOpacity onPress={() => this.props.navigation.navigate('Gibran')}>
                      <View style={{flexDirection:'row'}}> 
                        <Image source={require('../../image/gibran.png')} style={{height:100, width:100, marginTop:10, marginLeft:10, borderRadius:10}}/>
                        <View><Text style={{fontSize:20, marginTop:60, marginLeft:10, color:'black'}}>Gibran Rakabuming</Text></View>
                      </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View style={{flex:2,flexDirection:'row',borderColor:'black', justifyContent:'center'}}>
                    <View style={{height: 120, width: 355,backgroundColor: 'darkgrey',borderRadius: 10, borderColor:'black',borderEndWidth:8, marginTop:10}}>
                     <TouchableOpacity onPress={() => this.props.navigation.navigate('Nicholas')}>
                      <View style={{flexDirection:'row'}}> 
                        <Image source={require('../../image/nicholas.png')} style={{height:100, width:100, marginTop:10, marginLeft:10, borderRadius:10}}/>
                        <View><Text style={{fontSize:20, marginTop:60, marginLeft:10, color:'black'}}>Nicholas Kurniawan</Text></View>
                      </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View style={{flex:2,flexDirection:'row',borderColor:'black', justifyContent:'center'}}>
                    <View style={{height: 120, width: 355,backgroundColor: 'darkgrey',borderRadius: 10, borderColor:'black',borderEndWidth:8, marginTop:10}}>
                     <TouchableOpacity onPress={() => this.props.navigation.navigate('Sukyanto')}>
                      <View style={{flexDirection:'row'}}> 
                        <Image source={require('../../image/sukyanto.png')} style={{height:100, width:100, marginTop:10, marginLeft:10, borderRadius:10}}/>
                        <View><Text style={{fontSize:20, marginTop:60, marginLeft:10, color:'black'}}>Sukyatno Nugroho</Text></View>
                      </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View style={{flex:2,flexDirection:'row',borderColor:'black', justifyContent:'center'}}>
                    <View style={{height: 120, width: 355,backgroundColor: 'darkgrey',borderRadius: 10, borderColor:'black',borderEndWidth:8, marginTop:10}}>
                      <TouchableOpacity onPress={() => this.props.navigation.navigate('Sunny')}>
                      <View style={{flexDirection:'row'}}> 
                        <Image source={require('../../image/sunny.png')} style={{height:100, width:100, marginTop:10, marginLeft:10, borderRadius:10}}/>
                        <View><Text style={{fontSize:20, marginTop:60, marginLeft:10, color:'black'}}>Sunny Kamengmau</Text></View>
                      </View>
                      </TouchableOpacity>
                    </View>
                  </View>
                </ScrollView>
               </View>
            </View>
    )
  }
}

export default Home