import { Text, View, ScrollView } from 'react-native'
import React, { Component } from 'react'

export class Dea extends Component {
  render() {
    return (
      <View>
        <ScrollView>
        <Text style={{fontSize:20}}>Biografi Dea Valencia Pemilik Batik Kultur</Text>
        <Text>Dea Valencia adalah alumni Sistem Informasi angkatan 2009 Universitas Multimedia Nusantara. Dea menjual baju batik non printing, sehingga memiliki keunikan disetiap produknya.
        Berawal dari kecintannya terhadap kain batik, menjadi sebuah bisnis yang menguntungkan. Inilah salah satu bukti nyata mengubah passion menjadi sumber pendapatan.
        Menurut Dea, awalnya dia suka mengoleksi baju-baju batik yang antik alias kuno. Suatu saat dia memiliki ide menjual baju batik dengan gaya yang khas. Menurut Finansialku salah satu ciri khas produknya adalah corak batik yang dibawa kebaju-baju modern. 
Dea mengawali penjualan batik pada saat kuliah di semester tiga. Sama seperti mahasiswa/I lain yang menambah penghasilan dengan berjualan. Awalnya penjualan melalui Facebook dan hanya 20 potong pakaiaan. Saat ini jangan ditanya penjualannya mampu mencapai 600 potong per bulan. Berbicara mengenai pemasaran, Dea sering kali menggunakan foto pribadinya untuk memasarkan produk-produk di internet. Mungkin saja Dea berprinsip pengusaha yang bangga terhadap produknya: “Jika saya yang punya aja ga pake, kenapa orang lain harus beli?”
Batik kultur sendiri pada awalnya tidak berjalan mulus. Dea pernah berusan dengan masalah hak paten dalam nama. Nama adalah salah satu kunci dalam berbisnis. Memilih nama yang cocok untuk bisnis, menjadi tantangan tersendiri bagi para pemilik usaha. Dea pada awalnya mengusung nama batik Sinok, ternyata batik Sinok sudah didaftarkan oleh orang lain. Dea harus mengubah merknya dan terjadilah batik kultur. Nah inilah cerita awalnya Dea Valencia pemilik Batik 
Salah satu cerita dari kisah sukses Dea Valencia pemilik Batik Kultur adalah orang-orang yang membantu Dea memproduksi batik Kultur. Dea mempekerjakan orang-orang yang disfabel untuk membantunya memproduksi batik kultur. 
Salah satu idenya adalah Dea mempekerjakan teman-teman disfabel sebagai salah satu wujud kepedulian sosialnya.
        </Text>
        <Text style={{fontSize:20}}>Pelajaran dari Kisah Dea Valencia   </Text>
        <Text>Beberapa pelajaran yang dapat kita pelajari dari kisah sukses Dea Valencia pemilik batik kultur:</Text>
        <Text>1. Tidak ada jalan pintas menjadi sukses. There is no elevator to success you have to take the stairs.</Text>
        <Text>2. Pebisnis harus bangga terhadap produknya. Kalo saya sendiri ga mau pakai, kenapa orang lain harus beli.</Text>
        <Text>3. Mencari nama untuk bisnis baru cukup repot, tetapi mencari nama adalah pekerjaan penting.</Text>
        <Text>4. angan lupa dengan sesama.</Text>
        <Text></Text>
        <Text></Text>
        <Text></Text>
        <Text></Text>
        </ScrollView>
      </View>
    )
  }
}

export default Dea