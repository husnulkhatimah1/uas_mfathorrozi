import { Text, View, ScrollView } from 'react-native'
import React, { Component } from 'react'

export class Nicholas extends Component {
  render() {
    return (
      <View>
        <ScrollView>
        <Text style={{fontSize:20}}>Kisah Inspiratif Nicholas Kurniawan, Eksportir Ikan Hias Sukses Termuda di Indonesia</Text>
        <Text>Layaknya anak muda inspiratif lainnya, Nicholas Kurniawan membuka pintu sukes usaha ikan hias bukan dari warisan sukses keluarganya, namun dengan kerja keras, kerja cerdas dan tentu saja ada “key point” pendorong dia untuk sukses. 
        Lahir dalam keluarga sederhana, bahkan untuk biaya hidup sehari-hari, orang tua Nicholas Kurniawan harus berutang kesana kemari untuk menyambung hidup dan juga menyekolahkan anak-anaknya. Satu hal yang tak pernah dilupakan Nicholas adalah saat dia harus menerima “surat cinta” tagihan pembayaran uang sekolah dia yang tertunggak. Kondisi ini yang membuat dia harus mandiri lebih awal. Prinsip yang dia pegang, bisa membantu orang tua membiayai sekolah sendiri, apapun yang menghasilan asalkan halal dan bisa dijual akan dia jual.
Ditengah keluarga yang serba kekurangan, Nicholas sebenarnya anak yang cerdas dan berbakat. Menjadi lulusan terbaik di SD Santa Maria Djuanda, meraih nilai tertinggi matematika sejak SMP hingga SMA kelas 1, dan aktif dalam kegiatan OSIS dan ekstrakurikuler membuat Nicholas dikenal luas dikalangan teman-temannya.
Menjual makanan, minuman, pakaian, bisnis MLM, hingga asuransi semua sudah dia jalani sejak kelas 2 Sekolah dasar untuk membantu orang tuanya. Suka-duka, jatuh bangun dalam berjualan telah membentuk mental Nicholas menjadi tegar hingga sekarang.
Awal mula kisah sukses Nicholas berawal dari kebetulan. Dalam bisnis faktor kebetulan bahkan keberuntungan sekalipun hampir selalu menghampiri orang yang siap, sehingga tidak ada orang sukses yang datang tiba-tiba, begitupun awal sukses Nicholas di bisnis ikan hias.
Saat Nicholas duduk di bangku SMA kelas 2 , salah seorang  teman baiknya memberikan sepaket ikan Garra Rufa, ikan yang biasanya banyak dijumpai di mall untuk terapi. Entah apa tujuannya, walaupun temannya tahu, Nicho bukanlah orang yang suka memelihara ikan. Kisah sukses ternyata berawal dari sini.
Insting bisnis Nicholas langsung merespon. Prinsip apa saja yang bisa dijual akan dia jual membawa ikan hias tersebut coba ditawarkan dalam forum jual-beli Kaskus, awalaupun awalnya hanya iseng. Ajaib, ternyata laris. ikan yang ia jual tersebut banyak yang berminat. 
Kegagalan berulang di bisnis masa lalunya kini menjadi pondasi membentuk pola sukses di bisnis ikan hias ini. Segera, Nicholas mencari informasi profil ikan Garra Rufa termasuk tempat membeli ikan ini, sampai akhirnya dia membuka “Garra Rufa Center”, toko online khusus menjual ikan Garra Rufa beserta perlengkapannya. 
Profit bersih Rp2-3 juta per bulan untuk sebuah usaha awal tentu cukup lumayan dan bisa jadi motivasi sukses selanjutnya. Apalagi pelanggannya sudah masuk ke berbagai kalangan nulai dari anggota DPR, pengusaha besar hingga artis.
Selepas dari SMA, salah satu impian Nicholas adalah bisa kuliah di Prasetiya Mulya Business School. Biaya kuliah disini tentu tidaklah murah. Kebutuhan biaya 10 juta per bulan membuat otak kreatif Nicholas langsung merespon bahwa usaha dagang ikan hias tak akan cukup untuk mencukupi biaya tersebut.
Motivasi kuat agar bisa kuliah di  Prasetiya Mulya Business School telah mendorong kreativitas dan keberanian Nicholas untuk “naik kelas” dari pedagang ikan hias mencoba menjadi eksportir ikan hias. Dunia ekspor merupakan ilmu baru bagi Nicholas.
Dunia ekspor ternyata rumit pada awalnya. Butuh usaha ekstra untuk mendalami seluk beluk cara mempromosikan bisnis, media iklan yang digunakan, mencari supplier yang baik hingga informasi tentang shipment agent termasuk didalamnya segala dokumen untuk keperluan ekspor harus dikuasai dengan baik dan juga paham alur proses yang dijalankan.
Layaknya anak muda yang melek teknologi, Nicholas memasarkan ikan hiasnya melalui website yang ia bangun dengan nama Tropical Fish Indonesia. Tropical Fish merupakan kata kunci populer bagi penggemar ikan berburu produk ikan di dunia maya.
Nicholas menyadari, makin besar skala usaha dan promosi yang dia lakukan harus didukung suplier yang baik agar arus pesanan pembeli bisa tercukupi sesuai pesanan. Ratusan praposal kerjasama dia tawarkan, dan setelah sekian lama berusaha membangun kepercyaan, banyak pemasok dari sejumlah penangkar di Pulau Jawa, Kalimantan hingga Papua menjadi mitranya.
Kesuksesan yang tercipta dan terpola dari jerih lelah dan kerja keras, membawa Nicholas Kurniawan dinobatkan sebagai peraih juara satu Nasional Wirausaha Muda Mandiri 2013 dan dikenal sebagai eksportir ikan hias sukses termuda di Indonesia. Venus Aquatics adalah brand yang diusung oleh Nicholas dalam menembus ketatnya pasar ekspor dunia yang membuat dia sukses meraup omzet ratusan juta rupiah setiap bulannya dari bisnis ikan hias.
Virus kesuksesannya dalam berbisnis ini, ingin dia tularkan kepada adik-adik SMA agar mereka lebih mudah untuk bisa lebih sukses melalui pola sukses yang sudah dia rintis dengan cara membentuk Synergy Entrepreneur Academy sebagai wadah bersama untuk berbagi sukses dalam bisnis khususnya dalam memberikan workshop bisnis startup bagi para siswa SMA. Dari sini, Nicholas berharap ia mampu mencetak 5 juta pengusaha baru nantinya. 
</Text>
        <Text></Text>
        <Text></Text>
        <Text></Text>
        <Text></Text>
        <Text></Text>
        <Text></Text>
        <Text></Text>
        <Text></Text>
        <Text></Text>
        <Text></Text>
        <Text></Text>
        <Text></Text>
        <Text></Text>
        <Text></Text>
        <Text></Text>
        </ScrollView>
      </View>
    )
  }
}

export default Nicholas