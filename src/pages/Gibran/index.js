import { Text, View, ScrollView } from 'react-native'
import React, { Component } from 'react'


export class Gibran extends Component {
  render() {
    return (
      <View>
        <ScrollView>
        <Text style={{fontSize:20}}>Siapakah Gibran Rakabuming Raka?</Text>
        <Text>Setelah lulus dari University of Technology Sydney, Australia pada tahun 2010, Gibran Rakabuming Raka atau yang akrab disapa Gibran kembali ke Solo dan memulai usaha kuliner.
Gibran memulainya dengan merintis catering Chilli Pari kemudian merambah pula ke kuliner martabak yang dinamai Markobar.
Siapa sangka putra sulung seorang Presiden Jokowi dengan begitu rendah hati merintis sebuah usaha kuliner? Penasaran dengan kisahnya? Finansialku akan mengisahkan kesuksesan Gibran Rakabuming Raka dalam usaha martabaknya.
</Text>
        <Text style={{fontSize:20}}>Kisah Awal Tercetusnya Ide Markobar</Text>
        <Text>Gibran Rakabuming Raka, putra sulung Jokowi kelahiran tahun 1987 ini memulai usaha kuliner pada tahun 2010. Gibran terkenal dengan jiwa entrepreneur-nya yang ulet dan tegas, sehingga tak pernah terpikir oleh Gibran untuk mendompleng nama besar ayahnya untuk kesuksesan usahanya.
        Berbeda dengan keinginan orangtuanya yang meminta Gibran untuk berbisnis di bidang properti, dia memutuskan untuk memulai usaha kuliner dengan melihat potensi besar terhadap bisnis ini di Solo.
        Gibran pun tidak mengandalkan orangtuanya dalam urusan modal, dia meminjam sejumlah uang dari bank untuk memulai usaha catering-nya.
        Setelah sukses menjalani usaha catering, Gibran berpikir untuk merambah dunia kuliner dan membuka usaha martabak dengan berbagai varian topping yang dinamai Markobar.
        Markobar mulai dirintis di Solo pada tahun 2015. Markobar kini sudah berkembang dan dapat dinikmati juga di Jakarta, Semarang, dan Yogyakarta.
        Bekerja sama dengan mitra kerjanya, Gibran mempercayakan urusan dapur kepada mitranya, dan dia menjalankan segi pemasarannya dan membawanya ke kota-kota besar di Indonesia.
        
        </Text>
        <Text style={{fontSize:20}}>Kesuksesan Usaha Martabak Gibran Rakabuming Raka</Text>
        <Text>Gibran, yang kini sudah menjabat sebagai ketua Asosiasi Perusahaan Jasa Boga Indonesia (APJBI) Kota Solo ini beranggapan bahwa usaha martabaknya memiliki potensi besar untuk terus dikembangkan.
        Rata-rata penjualan martabak Markobar saat ini adalah 150-200 loyang dalam sehari dan dibandrol harga mulai dari Rp45.000 untuk 1 topping dan mencapai Rp90.000 untuk 8 topping per loyang. Harganya dipasang cukup terjangkau agar menjangkau pasar yang lebih luas.
        Gibran menentukan sebuah target setiap kali membuka sebuah gerai baru, yaitu seluruh modal yang diinvestasikan harus kembali dalam waktu maksimal 3 bulan. Gibran juga merencanakan untuk membuka 2 gerai Markobar setiap 2 bulannya untuk pengembangan selanjutnya.

        </Text>
        <Text style={{fontSize:20}}>Meski Dicemooh, Gibran Tetap Maju</Text>
        <Text>Usaha martabak dengan berbagai varian topping milik putra sulung Presiden RI periode 2014-2019 ini sempat menghebohkan dunia maya.
        Seorang netizen bahkan mencemoohnya dengan sebuah komentar pada akun Twitter-nya yang mengatakan bahwa usaha jual beli martabaknya adalah usaha yang kampungan.
        “Nurun ke anaknya pinjam dana bank Rp1 miliar cuma buat jualan martabak, kampungan,” tulis netizen tersebut dalam akun Twitter yang saat ini sudah berstatus non-aktif.
        tidak ambil pusing, Gibran bahkan mengacuhkan cemoohan tersebut.Yang membuat heboh malah adik kandung Gibran, Kaesang Pangarep yang menanggapi sindiran tersebut dengan santai, “Gapapa, yang penting dari jualan martabak bisa untuk bayar sekolah di Singapore. Saya hepi, Markobar juga hepi,”.
        Kaesang tidak mengambil hati sindiran tersebut, malah memanfaatkannya sebagai ajang promosi dengan beberapa tweet bernada sarkastik:    
        </Text>
        <Text style={{fontSize:20}}>Langkah Gibran Rakabuming Raka Selanjutnya</Text>
        <Text>Melihat kesuksesan usaha martabak Markobar, Gibran berencana untuk membuka gerai baru di Bali, Bandung, Manado, dan menambah gerai baru di Jakarta.
        Pada tahun 2016 dia juga membuka usaha baru di bidang kedai kopi yang dinamai CS Coffee Shop yang kini sudah memiliki gerai di Solo, Semarang, dan Yogyakarta.
        Gibran juga memiliki usaha Ceker Ayam Bakar dan seolah tidak mengenal lelah, dia berencana untuk memulai usaha barunya, yaitu Pasta Buntel dalam waktu dekat.
        Dia mengungkapkan bahwa seluruh usahanya merupakan usaha yang dijalaninya bersama rekan-rekannya, dimana dia lebih berperan di bagian pemasarannya.
        Namun dia juga tidak menutup kemungkinan untuk menambah beberapa usaha lagi jika terdapat prospek yang cerah dalam usaha tersebut. Kerja keras dan ketekunan Gibran Rakabuming tentunya patut diacungi jempol dan dijadikan panutan bagi Anda yang ingin memulai usaha.
        </Text>
        <Text></Text>
        </ScrollView>
      </View>
    )
  }
}

export default Gibran