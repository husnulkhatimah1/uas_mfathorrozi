import { ScrollView, Text, View } from 'react-native'
import React, { Component } from 'react'

export class Nikola extends Component {
  render() {
    return (
      <View>
        <ScrollView>
        <Text>Detikers pasti sudah nggak asing lagi dengan mobil listrik merek Tesla, tetapi tahukah kamu kalau nama ini terinspirasi dari seorang ilmuwan jenius.Yap, ilmuwan yang menginspirasi Elon Musk untuk memberi nama perusahannya tersebut adalah Nikola Tesla.
Nikola Tesla adalah sosok ilmuwan dan penemu yang berpengaruh dalam sejarah kehidupan umat manusia. Listrik yang kita nikmati sekarang merupakan salah satu penemuan jenius Nikola Tesla.
Apa yang telah dilakukan oleh Nikola Tesla merupakan sebuah hal yang sangat memengaruhi kehidupan manusia ke depannya. Nah, untuk mengenal Nikola Tesla lebih dalam, mari simak ceritanya! </Text>
<Text style={{fontSize:20}}>Siapakah Nikola Tesla </Text>
<Text>Namanya memang kalah beken kalau kita melakukan pembicaraan tentang orang jenius yang pernah ada di dunia. Nikola Tesla kalah populer dari sosok jenius lainnya seperti Thomas Alva Edison, Albert Einstein, atau Stephen Hawking.
Namun, kejeniusan ilmuwan underrated ini juga nggak bisa dipandang remeh. Penemuannya sangat berpengaruh besar terhadap kehidupan manusia sampai saat ini.
Berkat penemuannya, listrik dapat mengalir dan dapat menjadi sumber energi yang menghidupi berbagai macam aspek kehidupan manusia. Coba bayangkan kalau tidak ada listrik, kehidupan ini pasti diselimuti dengan kegelapan.
Penemuannya terhadap sistem arus bolak-balik atau Alternating Current (AC) menjadi batu lompatan dalam sistem listrik modern. Nikola Tesla juga menemukan penemuan penting lainnya seperti radio, remote control, motor listrik, dan trafo tegangan tinggi.
</Text>
<Text style={{fontSize:20}}>Biografi Nikola Tesla</Text>
<Text>Nikola Tesla lahir pada tanggal 10 Juli 1856 di Smiljan, Kekaisaran Austria, kini menjadi negara Kroasia. Dia merupakan seorang ilmuwan, fisikawan, teknisi mekanik, dan teknisi listrik yang menemukan konsep listrik bolak-balik yang menjadi cikal bakal sistem listrik modern.
Tesla sempat tinggal di New York dan bekerja untuk Thomas Alva Edison, ketika itu Tesla berhasil merancang 24 jenis dinamo. Namun, keduanya tak pernah cocok satu sama lain sehingga Tesla mendirikan laboratoriumnya sendiri.
Setelah berpisah dengan Edison, Tesla mampu menunjukkan kehebatannya dengan menemukan konsep arus bolak-balik (AC). Penemuannya itu lebih berat dibandingkan sistem listrik searah (DC) milik Edison.
Dalam kurun waktu setahun saja, Tesla telah mampu membuat 30 karya yang dijadikan hak paten oleh dirinya. Trafo tegangan tinggi, remote control, dan motor listrik juga merupakan contoh dari kontribusi penemuan Nikola Tesla.
Selain itu, Nikola Tesla juga melakukan serangkaian penelitian dan eksperimen tentang gelombang radio hingga berhasil membuat radio. Dia meneliti hal ini karena memiliki ketertarikan yang besar dengan radiasi elektromagnetik.
Namun sayangnya, Nikola Tesla gagal mematenkan radio dan akhirnya radio berhasil dipatenkan oleh Guglielmo Marconi sehingga kita mengenalnya sebagai penemu radio. Kegagalannya ini karena serangkaian musibah yang menimpanya, pada tahun 1895, laboratoriumnya dilahap habis oleh api sehingga banyak catatan penemuannya yang ikut terbakar, termasuk catatan tentang radio.
Meski dirinya menemukan banyak sekali penemuan yang berguna, namun Tesla tidak dihormati semasa hidupnya. Ide-idenya kala itu dianggap aneh, bahkan Nikola Tesla sering mendapat predikat sebagai orang gila.
Kemalangan Tesla terus berlanjut, pada tahun 1915, dia mengumumkan kebangkrutannya. Media saat itu memberitakan dirinya sebagai orang gila dan penipu.
Tesla pun menghabiskan akhir hidupnya dengan menggarap beberapa penelitian baru. Dia juga menghabiskan sisa hidupnya dengan merpati yang dipeliharanya, sampai-sampai di mengaku kalau dia bisa berkomunikasi dengan merpati.
</Text>
<Text style={{fontSize:20}}>Penemuan Nikola Tesla</Text>
<Text>1. Sistem Arus Listrik Alternating Current (AC)</Text>
<Text>Sistem ini kita kenal juga dengan sebutan arus bolak-balik. Penemuannya ini digunakan oleh para insinyur listrik sampai saat ini untuk mengalirkan listrik.</Text>
<Text>2. Pembangkit Listrik Tenaga Air</Text>
<Text>Nikola Tesla berhasil menemukan teknologi ini dengan memanfaatkan kekuatan dari air terjun. Tesla mengembangkan proyek transmisi AC hingga berhasil membangun pembangkit listrik tenaga air pertama di dunia.</Text>
<Text>3. Grafik Bayangan (The Shadowgraph)</Text>
<Text>Penemuannya ini terinspirasi dari penemuan sinar-x dari ilmuwan Jerman, Wilhelm Conrad Röntgen. Tesla mengembangkan shadowgraph dengan menggunakan tabung vakum dan menciptakan gambar yang lebih jelas dari teknologi sebelumnya.</Text>
<Text>4. Radio</Text>
<Text>Meskipun teknologi ini diklaim oleh Guglielmo Marconi, namun orang yang pertama kali mengembangkan teknologi radio adalah Nikola Tesla. Banyak sumber telah menyebutkan bahwa Marconi banyak mengambil ide dari Tesla hingga akhirnya mematenkan radio.</Text>
<Text>5. Lampu Neon</Text>
<Text>Lampu neon adalah modifikasi dan pengembangan yang dilakukan Nikola Tesla dari teknologi lampu yang sudah ada sebelumnya. Saat ini, lampu neon digunakan sebagai alat penerangan dari kegelapan di malam hari.</Text>
<Text>6. Motor Induksi</Text>
<Text>Motor ini menggunakan induksi elektromagnetik sehingga mampu berjalan. Motor induksi Tesla saat ini mampu menggerakan barang rumah tangga seperti penyedot debu, perkakas listrik, hingga pengering rambut.</Text>
<Text style={{fontSize:20}}>Kematian Nikola Tesla</Text>
<Text>Tesla meninggal dunia pada 7 Januari 1943, Nikola Tesla ditemukan meninggal dunia akibat serangan jantung. Dia ditemukan sendirian di kamar rumahnya yang berada di daerah New York.
Jasad Tesla baru ditemukan setelah dua hari kematiannya. Penemuannya ini karena seorang pelayan mendorong pintu hotel yang bertuliskan "Jangan Ganggu" lalu menemukannya dalam keadaan sudah mati.
Sebelum kematian yang mengenaskan, Tesla sempat menulis surat untuk ibunya, Djuka. Surat ini sangat pendek, hanya berisi dua kalimat, tetapi isinya begitu menyayat hati.
"Saya berharap saya bisa berada di sampingmu sekarang ibu, untuk membawakanmu segelas air. Bertahun-tahun yang saya habiskan untuk melayani umat manusia tidak membawa apa-apa selain hinaan."
</Text>
</ScrollView>
      </View>
    )
  }
}

export default Nikola