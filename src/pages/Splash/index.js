import { Text, View, Image } from 'react-native'
import React, { Component, useEffect } from 'react'

const Splash = ({navigation}) => {
    useEffect(() => {
        setTimeout(() => {
            navigation.replace('Home');
        }, 2000)
    })
    return (
      <View style={{flex:1, alignContent:'center',justifyContent:'center'}}>
        <Image source={require('../../image/logo.png')} style={{marginLeft:30}}/>
      </View>
    )
  }


export default Splash