import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { Splash, Home, Nikola, Bob, Susi, Reza, James, Dea, Gibran, Nicholas, Sukyanto, Sunny } from "../src/pages";

const Stack = createNativeStackNavigator();

const Router = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen name="Splash" component={Splash} options={{headerShown:false}}/>
            <Stack.Screen name="Home" component={Home}/>
            <Stack.Screen name="Nikola" component={Nikola}/>
            <Stack.Screen name="Bob" component={Bob}/>
            <Stack.Screen name="Susi" component={Susi}/>
            <Stack.Screen name="Reza" component={Reza}/>
            <Stack.Screen name="James" component={James}/>
            <Stack.Screen name="Dea" component={Dea}/>
            <Stack.Screen name="Gibran" component={Gibran}/>
            <Stack.Screen name="Nicholas" component={Nicholas}/>
            <Stack.Screen name="Sukyanto" component={Sukyanto}/>
            <Stack.Screen name="Sunny" component={Sunny}/>
        </Stack.Navigator>
    )
}

export default Router;