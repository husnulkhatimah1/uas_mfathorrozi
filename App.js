import React from 'react';
import { Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import Router from './Route';

const YourApp = () => {
  return (
    <NavigationContainer>
      <Router />
    </NavigationContainer>
  );
}; 

export default YourApp;